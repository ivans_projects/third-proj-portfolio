var regexEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
var textfield = $('.pageform__textfield');
var emailField =$('.pageform__emailfield');
var textareafield = $('.pageform__textareafield');
var arrowUpButton = $("#arrowup");
var knowMoreButton = $('#knowMoreButton');
var knowMoreSection = $('.aboutsection');
var hireMeButton = $('#hireMeButton');
var hireMeSection = $('.contactform');

$(document).ready(function() {
    var text_max = 250;
    $('#textarea_feedback').html(text_max + ' chars left');
    textareafield.keyup(function() {
        var text_length = textareafield.val().length;
        var text_remaining = text_max - text_length;
        $('#textarea_feedback').html(text_remaining + ' chars left');
    });
    arrowUpButton.on('click', function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
    hireMeButton.on('click', function(){
        $("html, body").animate({
        scrollTop: hireMeSection.offset().top + hireMeSection.outerHeight(true)
    }, 500);
         return false;
     });
      knowMoreButton.on('click', function(){
        $("html, body").animate({
        scrollTop: knowMoreSection.offset().top 
    }, 500);
         return false;
     });
     $('.owl-carousel').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
})
});

function validate() {

    if ( (textfield.val().length === 0) ){
       textfield.addClass('error');
    }  else {
        textfield.removeClass('error');
     }
     if ( !regexEmail.test(emailField)) {
        emailField.addClass('error');
     } else {
         emailField.removeClass('error');
     }
     if ((textareafield.val().length === 0)) {
         textareafield.addClass('error');
     }
     else {
        textareafield.removeClass('error');
     }
}

