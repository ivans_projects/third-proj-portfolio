var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
	autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
	rename = require('gulp-rename'),
	livereload = require('gulp-livereload'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	notify = require('gulp-notify'),
	imagemin = require('gulp-imagemin'),
    del = require('del');
	
gulp.task('styles', function() {
  return sass('sass/main.scss', { noCache: true, style: 'expanded' })
    .pipe(autoprefixer('last 4 version'))
    .pipe(gulp.dest('css/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('css/'))
    .pipe(notify({ message: 'Styles task complete' }))
	.pipe(reload({ stream:true }));
});
gulp.task('imagemin', function() {
		gulp.src('img/*')
			.pipe(imagemin())
			.pipe(gulp.dest('build/'));
});
gulp.task('watch',  ['browserSync'], function() {
  gulp.watch('sass/**/*.scss', ['styles']);
  livereload.listen();
  gulp.watch(['css/**']).on('change', livereload.changed);
  gulp.watch("*.html").on("change", reload);
});

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('default',['styles','imagemin','watch','browserSync']);